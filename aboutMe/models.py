from django.db import models

class PersonalInfo(models.Model):
    name            = models.CharField(max_length = 50)
    email           = models.CharField(max_length = 80)
    birth_place     = models.CharField(max_length = 100)
    date_of_birth   = models.DateTimeField('Date of Birth')
    phone_number    = models.CharField(max_length = 20)
    linkedin_url    = models.CharField(max_length = 200)
    github_url      = models.CharField(max_length = 200)
    instagram_url   = models.CharField(max_length = 200)
    facebook_url    = models.CharField(max_length = 200)
    brief_desc      = models.CharField(max_length = 200)
    description     = models.CharField(max_length = 500)
    life_motto      = models.CharField(max_length = 500)

    def __str__ (self):
        return self.name
