from django.shortcuts import get_object_or_404, render
from django.template import loader

from .models import PersonalInfo

from datetime import datetime, date

curr_year = int(datetime.now().strftime("%Y"))
curr_month = int(datetime.now().strftime("%m"))
curr_date = int(datetime.now().strftime("%d"))

def index(request):
    personalInfo = get_object_or_404(PersonalInfo, name = "Ricky Chandra Johanes")
    birth_date = date(1998, 11, 5)
    template = loader.get_template('aboutMe/index.html')
    context = {
        'name'          : personalInfo.name,
        'email'         : personalInfo.email,
        'age_year'      : calculate_age_year(birth_date.day, birth_date.month, birth_date.year),
        'age_month'     : calculate_age_month(birth_date.day, birth_date.month, birth_date.year),
        'age_day'       : calculate_age_day(birth_date.day, birth_date.month, birth_date.year),
        'birth_place'   : personalInfo.birth_place,
        'phone_number'  : personalInfo.phone_number,
        'linkedin_url'  : personalInfo.linkedin_url,
        'github_url'    : personalInfo.github_url,
        'instagram_url' : personalInfo.instagram_url,
        'facebook_url'  : personalInfo.facebook_url,
        'description'   : personalInfo.description,
        'life_motto'    : personalInfo.life_motto,

    }
    return render(request, 'aboutMe/index.html', context)

def calculate_age_year (birth_date, birth_month, birth_year) :
    if ((curr_month > birth_month) or (curr_date == birth_date and curr_month == birth_month)) :
        return curr_year - birth_year
    else :
        return curr_year - birth_year - 1

def calculate_age_month (birth_date, birth_month, birth_year) :
    result = 0;
    if curr_month == birth_month :
        return result;
    else :
        month = birth_month
        while month != curr_month :
            if month < 12 :
                month += 1
                result += 1
            else :
                month = 0
                result += 1

        if (curr_date <= birth_date) :
            result -= 2

    return result

def calculate_age_day (birth_date, birth_month, birth_year) :
    result = 0;
    if (curr_date == birth_date):
        return result
    else :
        date = birth_date
        while date != curr_date :
            if (curr_month == 1 or curr_month == 3 or curr_month == 5 or curr_month == 7 or curr_month == 8 or curr_month == 10 or curr_month == 12):
                if date < 31:
                    date += 1
                    result += 1
                else :
                    date = 0
                    result += 1
            elif (curr_month == 2):
                if (curr_year % 4) == 0 :
                    if date < 29:
                        date += 1
                        result += 1
                else :
                    if date < 28:
                        date += 1
                        result += 1
            else :
                if date < 30:
                    date += 1
                    result += 1
                else :
                    date = 0
                    result += 1

        if (curr_date <= birth_date) :
            result -= 2

    return result
